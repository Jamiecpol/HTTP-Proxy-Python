import socket, sys
from _thread import *

try:
	listen_port = int(input("Please Enter Listening Port No. : "))
except KeyboardInterrupt:
	print("\n Application Exiting Now.")
	sys.exit()


MAX_NUM_CONNECTIONS = 5 # mac number of connections we accept
BUFFER_SIZE = 8192
DEFAULT_PORT = 80
HTTP_INDEX = 4


def main(): 
	try: 
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print("\n Socket created.")
		s.bind(('', listen_port))
		print("\n Socket binded to port.")
		s.listen(MAX_NUM_CONNECTIONS)
		print("\n Listening now...")
	except Exception as e:
		#socket has failed
		print("Socket unable to initialize. Exiting Now.")
		sys.exit(2)


	while 1:
		try:
			connection, addr = s.accept() #Accept client connection
			print("\n Client connection accepted.")
			data = connection.recv(BUFFER_SIZE) #Client data
			start_new_thread(request, (connection, data, addr)) #Beging a new thread, asynchronously executing thw function that will handle the browser request
		except KeyboardInterrupt :
			s.close()
			print("\n User cancelled.")
			print("\n Proxy server shutting down.")
			sys.exit(1)

	s.close()


def request(connection, data, addr):
	#browser request 
	try:
		###PLACEHOLDER until I figure out a better way to do this with regex
		first_line = data.splitlines()[0]
		print(first_line)

		url = first_line.split()[1]
		print(url)


		
		temp = url[(HTTP_INDEX + 3) :]
		print(temp)

		port_index = temp.decode('utf-8').find(":")


		server_index = temp.decode('utf-8').find("/")


		server = ""
		port = -1

		if (port_index == -1 or server_index < port_index):
			print("Initalizing to default port.")
			port = DEFAULT_PORT
			print("uwu")
			server = url

		else:
			print("Port was specifided. Initializing to specified port.")
			port = int( temp[ (port_index + 1) : server_index] )
			print("so far so good...")
			server = temp.decode('utf-8')[:port_index]

		proxy_serv(server, port, connection, addr, data)


	except Exception as e:
		print("\n Error retrieving browser request!")
		print(e)
		sys.exit(2)
		

def proxy_serv(webserv, port, connection, data, addr):
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((webserv, port))
		s.send(data)
                #Connection to the server established. 

		while 1: 
			reply = s.recv(BUFFER_SIZE)
			connection.send(reply)

		s.close()
		connection.close()
	except socket.error:
		s.close()
		connection.close()
		sys.exit(1)


main()
